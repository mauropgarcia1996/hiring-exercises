const fs = require("fs");
const Stripe = require("stripe");
const STRIPE_TEST_SECRET_KEY =
  "rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR";
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

// This function is basically to loop through users and check if the country
// parameter matches with the user.country value. Then it pushes that user
// to a array, returning it.
const filterByCountry = (country, users) => {
  let userArray = [];
  for (let i = 0; i < users.length; i++) {
    let user = users[i];

    if (country == user.country) {
      userArray.push(user);
    }
  }
  return userArray;
};

// We use another loop to check a match on countryISO and provided countries values.
// We then return the countryISO (e.g. 'ES') to be used then.
const setISO = (countriesArray, country) => {
  let countryISO = "";
  for (let i = 0; i < countriesArray.length; i++) {
    if (countriesArray[i][1] == country) {
      let countrySelectedCode = countriesArray[i][0];
      countryISO = countrySelectedCode;
    }
  }
  return countryISO;
};

// This is just the Stripe () to create a Customer via the API. We return the async function
// to then use the result in any place.
const createStripeCustomer = async (user) => {
  return await stripe.customers.create({
    email: user.email,
    name: user.first_name + " " + user.last_name,
    address: {
      line1: user.address_line_1,
      country: user.country,
    },
  });
};

const handler = async (country) => {
  try {
    let finalCustomers = [];

    /* add code below this line */
    const countriesISO = fs.readFileSync("countries-ISO3166.json");
    const customers = fs.readFileSync("customers.json");
    let customersFilteredByCountry = [];
    const countries = JSON.parse(countriesISO);
    const countriesArray = Object.entries(countries);
    const users = JSON.parse(customers);

    // filter the customers by country
    customersFilteredByCountry = filterByCountry(country, users);

    // exchange country name for country code
    for (let i = 0; i < customersFilteredByCountry.length; i++) {
      // We set the customer country to the countryISO using our function.
      customersFilteredByCountry[i].country = setISO(countriesArray, country);
      let user = customersFilteredByCountry[i];

      // transform customers to save into Stripe
      // for each customer create a Stripe customer
      // We return the API result using our function to use it then in the finalCustomer Array.
      const stripeCustomer = createStripeCustomer(user);
      const result = await stripeCustomer;

      // push into finalCustomers the stripe customers with email, country and id as properties.
      let stripeNewCustomer = new Object();
      stripeNewCustomer.email = result.email;
      stripeNewCustomer.customerId = result.id;
      stripeNewCustomer.country = result.address.country;

      finalCustomers.push(stripeNewCustomer);
    }

    // write finalCustomers array into final-customers.json using fs
    /*
      finalCustomers array should look like:
      finalCustomers = [{
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        },
        {
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        }
      }]
    */
    let data = JSON.stringify(finalCustomers);
    fs.writeFile("final-customers.json", data, (err) => {
      if (err) {
        console.log(err);
      }
      console.log("File saved succesfully");
    });

    /* add code above this line */
    console.log(finalCustomers);
  } catch (e) {
    throw e;
  }
};

handler("Spain");
