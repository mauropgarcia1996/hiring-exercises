import React from "react";

const Title = ({ text }) => {
  return (
    <div>
      <h1 className="text-3xl font-bold pt-8 lg:pt-0">{text}</h1>
    </div>
  );
};

export default Title;
