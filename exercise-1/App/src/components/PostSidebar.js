import React from "react";
import PostSidebarLayout from "./PostSidebarLayout";
import Subtitle from "./Subtitle";
import PostTitle from "./PostTitle";

const PostSidebar = ({ data }) => {
  return (
    <PostSidebarLayout>
      <Subtitle text={data.user.name + "'s Posts:"} />
      <div>
        {data.user.posts.data.map((post) => (
          <PostTitle text={post.title} key={post.id} />
        ))}
      </div>
    </PostSidebarLayout>
  );
};

export default PostSidebar;
