import React from "react";

const Subtitle = ({ text }) => {
  return (
    <div>
      <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
        {text}
      </p>
    </div>
  );
};

export default Subtitle;
