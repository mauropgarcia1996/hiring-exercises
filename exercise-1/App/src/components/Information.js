import React from "react";
import Title from "./Title";
import Subtitle from "./Subtitle";
import Caption from "./Caption";
import Separator from "./Separator";

const Information = ({ data }) => {
  return (
    <div className="p-4 md:p-12 text-center lg:text-left">
      <Title text={data.user.name} />
      <Separator />
      <Subtitle text="Adress:" />
      <Caption
        text={
          data.user.address.street +
          " " +
          data.user.address.suite +
          " " +
          data.user.address.city
        }
      />
      <Subtitle text="Email:" />
      <Caption text={data.user.email} />
      <Subtitle text="Phone:" />
      <Caption text={data.user.phone} />
      <Subtitle text="Company:" />
      <Caption text={data.user.company.name} />
    </div>
  );
};

export default Information;
