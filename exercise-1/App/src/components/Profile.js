import React from "react";

const Profile = ({ children }) => {
  return (
    <div
      id="profile"
      className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0"
    >
      {children}
    </div>
  );
};

export default Profile;
