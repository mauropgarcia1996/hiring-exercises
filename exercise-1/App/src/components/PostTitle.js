import React from "react";

const PostTitle = ({ text, key }) => {
  return (
    <div>
      <p key={key}>{text}</p>
    </div>
  );
};

export default PostTitle;
