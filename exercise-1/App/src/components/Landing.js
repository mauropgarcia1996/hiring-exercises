import React, { useEffect } from "react";
import { useQuery, gql } from "@apollo/client";
import Loading from "./Loading";
import ErrorComponent from "./ErrorComponent";
import CardLayout from "./CardLayout";
import LandingLayout from "./LandingLayout";
import Profile from "./Profile";
import Information from "./Information";
import PostSidebar from "./PostSidebar";

const GET_USER = gql`
  {
    user(id: 1) {
      id
      username
      name
      email
      phone
      company {
        name
      }
      address {
        street
        city
        suite
        zipcode
      }
      posts(options: { paginate: { page: 1, limit: 10 } }) {
        data {
          id
          title
        }
      }
    }
  }
`;

const Landing = () => {
  const { loading, error, data } = useQuery(GET_USER);

  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  return (
    <CardLayout>
      <>
        <LandingLayout>
          <Profile>
            <Information data={data} />
          </Profile>

          <PostSidebar data={data} />
        </LandingLayout>
      </>
    </CardLayout>
  );
};

export default Landing;
