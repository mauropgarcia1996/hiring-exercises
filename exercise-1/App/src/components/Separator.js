import React from "react";

const Separator = () => {
  return (
    <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
  );
};

export default Separator;
