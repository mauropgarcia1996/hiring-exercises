import React, { useEffect } from "react";
import { useQuery, gql } from "@apollo/client";
import Loading from "./Loading";
import ErrorComponent from "./ErrorComponent";
import CardLayout from "./CardLayout";

const GET_USER = gql`
  {
    user(id: 1) {
      id
      username
      email
      address {
        geo {
          lat
          lng
        }
      }
      posts(options: { paginate: { page: 1, limit: 10 } }) {
        data {
          id
          title
        }
      }
    }
  }
`;

const ExampleComponent = () => {
  const { loading, error, data } = useQuery(GET_USER);

  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  return (
    <CardLayout>
      <>
        <div className="text-gray-900 font-bold text-lg mb-6">
          Example of API usage
        </div>
        <div className="text-gray-900 text-md mb-6">ID: {data.user.id}</div>
        <div className="text-gray-900 text-md mb-6">Name: {data.user.name}</div>
        <div className="text-gray-900 text-md mb-6">
          Name: {data.user.email}
        </div>
        <div>
          {data.user.posts.data.map((post) => (
            // <h1 key={post.id}>{post.title}</h1>
            <p
              key={post.id}
              class="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start"
            >
              Title: {post.title}
            </p>
          ))}
        </div>
      </>
    </CardLayout>
  );
};

export default ExampleComponent;
