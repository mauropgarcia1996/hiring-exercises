import React from "react";

const Caption = ({ text }) => {
  return (
    <div>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
        {text}
      </p>
    </div>
  );
};

export default Caption;
